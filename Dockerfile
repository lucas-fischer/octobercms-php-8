FROM php:8-apache

# Install dependencies
RUN apt-get update && \
    apt-get install -y \
        git \
        libzip-dev \
        libssl-dev \
        libfreetype6-dev \
        libjpeg62-turbo-dev \
        libpng-dev \
        libxml2-dev \
        libonig-dev \
        unzip && \
    # Install PHP extensions
    docker-php-ext-configure gd --with-freetype=/usr/include/ --with-jpeg=/usr/include/ && \
    docker-php-ext-install mbstring zip gd simplexml

RUN a2enmod rewrite

WORKDIR /var/www/html